<?php

declare(strict_types=1);

namespace App\Import;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

/**
 * @phpstan-type GlobalData array<string,mixed>
 * @phpstan-type Games array<string,mixed>[]
 */
final class YamlDatabase
{
    /**
     * @param  GlobalData  $global
     * @param  Games  $games
     */
    private function __construct(
        public readonly array $global,
        public readonly array $games
    ) {
    }

    public static function load(): self
    {
        return new self(
            self::readGlobalData(),
            self::readGamesData(),
        );
    }

    /**
     * @return GlobalData
     */
    private static function readGlobalData(): array
    {
        $data = self::parseYaml(
            Storage::disk('dumps')->get('yaml/database_main.yaml')
        );

        // Main database only contains a single array entry with all the data.
        return $data[0];
    }

    /**
     * @return Games
     */
    private static function readGamesData(): array
    {
        $storage = Storage::disk('dumps');

        $games = [];

        foreach ($storage->files('yaml') as $yamlFile) {
            if (
                File::extension($yamlFile) !== 'yaml'
                || $yamlFile === 'yaml/database_main.yaml'
            ) {
                continue;
            }

            $games = array_merge($games, self::parseYaml(
                Storage::disk('dumps')->get($yamlFile)
            ));
        }

        return $games;
    }

    /**
     * @return array<string,mixed>[]
     */
    private static function parseYaml(?string $yaml): array
    {
        if ($yaml === null) {
            throw new \UnexpectedValueException('Error reading yaml');
        }

        return Yaml::parseAsList($yaml);
    }
}

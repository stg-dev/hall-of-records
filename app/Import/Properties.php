<?php

declare(strict_types=1);

namespace App\Import;

final class Properties
{
    /** @var string[] */
    private array $fetched = [];

    /**
     * @param  array<string,mixed>  $properties
     */
    public function __construct(
        private array $properties
    ) {
    }

    public function fetchString(string $name): string
    {
        $value = $this->properties[$name];

        if (! is_string($value)) {
            throw new \UnexpectedValueException(
                "Property `{$name}` is expected to be a string"
            );
        }

        $this->markAsFetched($name);

        return $value;
    }

    public function fetchStringOrNull(string $name): ?string
    {
        if (! isset($this->properties[$name])) {
            return null;
        }

        return $this->fetchString($name);
    }

    private function markAsFetched(string $name): void
    {
        $this->fetched[] = $name;
    }

    public function remainingAsYaml(): ?string
    {
        $propertyNames = array_diff(
            array_keys($this->properties),
            $this->fetched
        );

        $remaining = array_reduce(
            $propertyNames,
            fn (array $all, string $name) => array_merge($all, [
                $name => $this->properties[$name],
            ]),
            []
        );

        if ($remaining === []) {
            return null;
        }

        return Yaml::dump($remaining);
    }
}

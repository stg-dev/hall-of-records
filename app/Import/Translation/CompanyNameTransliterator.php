<?php

declare(strict_types=1);

namespace App\Import\Translation;

final class CompanyNameTransliterator
{
    public function transliterate(RequiredTranslations $names): OptionalTranslations
    {
        return Translations::optional()
            ->add(Locale::En, null)
            ->add(Locale::Ja, $this->toHiragana($names->get(Locale::Ja)));
    }

    public function toHiragana(string $name): ?string
    {
        return match ($name) {
            'エーディーケイ' => 'えーでぃーけい',
            // @TODO Transliterate the others
            default => null
        };
    }
}

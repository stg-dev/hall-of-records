<?php

declare(strict_types=1);

namespace App\Import\Translation;

final class OptionalTranslations extends Translations
{
    /** @var array<string,string|null> */
    private array $translations = [];

    public function add(Locale $locale, ?string $value): self
    {
        $this->translations[$locale->value] = $value;

        return $this;
    }

    public function get(Locale $locale): ?string
    {
        return $this->translations[$locale->value];
    }
}

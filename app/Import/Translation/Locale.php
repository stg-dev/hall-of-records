<?php

declare(strict_types=1);

namespace App\Import\Translation;

enum Locale: string
{
    case En = 'en';
    case Ja = 'ja';
}

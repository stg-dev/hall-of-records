<?php

declare(strict_types=1);

namespace App\Import\Translation;

abstract class Translations
{
    public static function required(): RequiredTranslations
    {
        return new RequiredTranslations();
    }

    public static function optional(): OptionalTranslations
    {
        return new OptionalTranslations();
    }
}

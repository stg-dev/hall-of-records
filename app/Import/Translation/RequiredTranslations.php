<?php

declare(strict_types=1);

namespace App\Import\Translation;

final class RequiredTranslations extends Translations
{
    /** @var array<string,string> */
    private array $translations = [];

    public function add(Locale $locale, string $value): self
    {
        $this->translations[$locale->value] = $value;

        return $this;
    }

    public function get(Locale $locale): string
    {
        return $this->translations[$locale->value];
    }

    public function implode(): string
    {
        return collect($this->translations)->unique()->implode(', ');
    }
}

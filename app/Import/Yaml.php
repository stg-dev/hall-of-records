<?php

declare(strict_types=1);

namespace App\Import;

use Symfony\Component\Yaml\Yaml as SymfonyYaml;

final class Yaml
{
    /**
     * @return array<string,mixed>[]
     */
    public static function parseAsList(string $yaml): array
    {
        $data = SymfonyYaml::parse($yaml);

        if (! is_array($data)) {
            throw new \UnexpectedValueException(
                'Parsed yaml is expected to be an array'
            );
        }

        if (array_values($data) !== $data) {
            throw new \UnexpectedValueException(
                'Parsed yaml is expected to be a list'
            );
        }

        return $data;
    }

    /**
     * @return array<string,mixed>
     */
    public static function parseAsMap(string $yaml): array
    {
        $data = SymfonyYaml::parse($yaml);

        if (! is_array($data)) {
            throw new \UnexpectedValueException(
                'Parsed yaml is expected to be an array'
            );
        }

        if ($data !== [] && array_values($data) === $data) {
            throw new \UnexpectedValueException(
                'Parsed yaml is expected to be a map'
            );
        }

        return $data;
    }

    public static function dump(mixed $value): string
    {
        return SymfonyYaml::dump($value, 99, 4, SymfonyYaml::DUMP_OBJECT_AS_MAP);
    }
}

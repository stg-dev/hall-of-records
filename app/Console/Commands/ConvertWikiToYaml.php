<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Yaml\Yaml;

class ConvertWikiToYaml extends Command
{
    protected $signature = 'convert:wiki-to-yaml';

    protected $description = 'Convert wiki files to yaml';

    public function handle(): int
    {
        $storage = Storage::disk('dumps');

        foreach ($storage->files('wiki') as $wikiFile) {
            if (File::extension($wikiFile) !== 'wiki') {
                continue;
            }

            $yaml = $this->extractYaml(
                $storage->get($wikiFile)
            );

            $yamlFile = 'yaml/'.File::name($wikiFile).'.yaml';

            $storage->put($yamlFile, $yaml);
        }

        return Command::SUCCESS;
    }

    private function extractYaml(string $wikiData): string
    {
        preg_match_all('@<nowiki>(.*?)</nowiki>@us', $wikiData, $matches);

        $data = array_map(
            fn (string $yaml) => Yaml::parse($yaml),
            $matches[1]
        );

        return Yaml::dump($data, 99);
    }
}

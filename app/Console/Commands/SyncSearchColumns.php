<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\Game;
use Illuminate\Console\Command;

class SyncSearchColumns extends Command
{
    protected $signature = 'sync:search-columns';

    protected $description = 'Keep name_search in sync with translations';

    public function handle(): int
    {
        $records = [
            ...Game::query()->get(),
            ...Company::query()->get(),
        ];

        foreach ($records as $record) {
            $record->updateQuietly([
                /** @phpstan-ignore-next-line */
                'name_search' => $record->translations->pluck('name')->implode(', '),
            ]);
        }

        return 0;
    }
}

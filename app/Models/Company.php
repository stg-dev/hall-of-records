<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Company extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    /**
     * @var array<string>
     */
    public array $translatedAttributes = ['name'];

    protected $fillable = ['name', 'name_search'];

    public function games(): BelongsToMany
    {
        return $this->belongsToMany(Game::class);
    }
}

<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * @property int $id
 */
class Game extends Model implements TranslatableContract
{
    use HasFactory;
    use HasSlug;
    use Translatable;

    /**
     * @var array<string>
     */
    public array $translatedAttributes = ['name'];

    protected $fillable = ['name', 'name_search'];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(function ($game) {
                if ($name = $game->translations->where('lang', 'en')->first()) {
                    return $name;
                }

                return $game->name;
            })
            ->saveSlugsTo('slug');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function companies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class);
    }

    public function scores(): HasMany
    {
        return $this->hasMany(Score::class);
    }

    public function categories(): HasMany
    {
        return $this->hasMany(Category::class);
    }
}

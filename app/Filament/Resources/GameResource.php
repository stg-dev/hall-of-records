<?php

namespace App\Filament\Resources;

use App\Filament\Resources\GameResource\Pages;
use App\Filament\Resources\GameResource\RelationManagers\CategoriesRelationManager;
use App\Filament\Resources\GameResource\RelationManagers\ScoresRelationManager;
use App\Models\Game;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\IconColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Model;

class GameResource extends Resource
{
    protected static ?string $model = Game::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('companies')
                    ->relationship(name: 'companies', titleAttribute: 'name_search')
                    ->multiple()
                    ->searchable()
                    ->preload(),
                Forms\Components\TextInput::make('name')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('counterstop_score')
                    ->maxLength(255),
                Forms\Components\TextInput::make('counterstop_type')
                    ->required()
                    ->maxLength(255),
                Forms\Components\Toggle::make('needs_work')
                    ->required(),
                Forms\Components\Textarea::make('description')
                    ->maxLength(65535)
                    ->columnSpanFull(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('created_at')->dateTime()->toggleable(isToggledHiddenByDefault: true),
                TextColumn::make('updated_at')->dateTime()->toggleable(isToggledHiddenByDefault: true),
                TextColumn::make('name_search')->searchable()->label('Name'),
                TextColumn::make('counterstop_score'),
                TextColumn::make('counterstop_type'),
                TextColumn::make('categories_count')->counts('categories'),
                TextColumn::make('companies_count')->counts('companies'),
                TextColumn::make('companies')
                    ->state(function (Model $record): string {
                        /** @phpstan-ignore-next-line */
                        return $record->companies->pluck('name')->implode(', ');
                    }),

                IconColumn::make('needs_work')->boolean(),
            ])
            ->persistSortInSession()
            ->persistSearchInSession()
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            CategoriesRelationManager::class,
            ScoresRelationManager::class,
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListGames::route('/'),
            'create' => Pages\CreateGame::route('/create'),
            'edit' => Pages\EditGame::route('/{record}/edit'),
        ];
    }
}

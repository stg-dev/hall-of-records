<?php

namespace App\Filament\Resources\PlayerResource\RelationManagers;

use App\Models\Category;
use App\Models\Game;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Table;

class ScoresRelationManager extends RelationManager
{
    protected static string $relationship = 'Scores';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('game_id')
                    ->options(
                        Game::query()->pluck('name_search', 'id')
                    )
                    ->label('Game')
                    ->live(),
                Forms\Components\Select::make('category_id')
                    ->options(
                        fn (Forms\Get $get) => Category::query()
                            ->where('game_id', $get('game_id'))
                            ->pluck('name', 'id')
                    )
                    ->label('Catgory'),
                Forms\Components\TextInput::make('score')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('entry_name')
                    ->required()
                    ->maxLength(255),
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->recordTitleAttribute('score')
            ->columns([
                Tables\Columns\TextColumn::make('game.name_search'),
                Tables\Columns\TextColumn::make('category.name'),
                Tables\Columns\TextColumn::make('score'),
                Tables\Columns\TextColumn::make('entry_name'),
            ])
            ->filters([
                //
            ])
            ->headerActions([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                //
            ]);
    }
}

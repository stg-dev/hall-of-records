<?php

namespace App\Filament\Resources\GameResource\RelationManagers;

use App\Models\Category;
use App\Models\Score;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Table;

class ScoresRelationManager extends RelationManager
{
    protected static string $relationship = 'scores';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('category_id')
                    ->options(
                        fn (Score $score) => Category::query()
                            ->where('game_id', $score->game->id)
                            ->pluck('name', 'id')
                    ),
                Forms\Components\TextInput::make('score')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('entry_name')
                    ->maxLength(255),
                Forms\Components\Select::make('player_id')
                    ->relationship('player', 'name')
                    ->searchable(),
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->recordTitleAttribute('score')
            ->columns([
                Tables\Columns\TextColumn::make('category.name'),
                Tables\Columns\TextColumn::make('entry_name'),
                Tables\Columns\TextColumn::make('player.name'),
            ])
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }
}

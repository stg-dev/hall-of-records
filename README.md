## Hall of Records

[![pipeline status](https://gitlab.com/stg-dev/hall-of-records/badges/master/pipeline.svg)](https://gitlab.com/stg-dev/hall-of-records/-/commits/master)

Modern replacement for shmups.wiki written in laravel / filament.

Install using `make install`.

Demo admin: admin@example.com / password

Deployment using https://github.com/dg/ftp-deployment/

```bash
cp deployment.php.example deployment.php

make deploy

mysqldump hall_of_records -h 127.0.0.1 -u root -psecret > /tmp/dump.sql
# import at: https://v131299.kasserver.com/mysqladmin/PMA5/index.php
```


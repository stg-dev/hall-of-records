<?php

namespace Database\Seeders;

use App\Import\Yaml;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Nette\Utils\Json;

/**
 * @phpstan-type GlobalLayout array{
 *     group: array{
 *         scores: string[]
 *     }
 * }
 * @phpstan-type Scores array<string,mixed>[]
 * @phpstan-type GameYaml array{
 *     scores: Scores
 * }
 */
class CategorySeeder extends Seeder
{
    public function run(): void
    {
        $layout = $this->readGlobalLayout();

        $attributeNames = $this->readAttributeNames($layout);

        foreach ($this->readGames() as $gameId => $gameYaml) {
            $yamlData = $this->parseGameYaml($gameYaml);

            $categories = $this->readCategories(
                $attributeNames,
                $gameId,
                $yamlData['scores']
            );

            $this->insertCategories($categories);

            $this->updateGameYaml($gameId, $yamlData);
        }

        $this->updateGlobalLayout($layout);
    }

    /**
     * @param  \stdClass[]  $categories
     */
    private function insertCategories(array $categories): void
    {
        foreach ($categories as $index => $category) {
            $this->insertCategory($category);
        }
    }

    private function insertCategory(\stdClass $category): void
    {
        $attributes = $category->attributes;

        $name = collect($attributes)->implode('-');

        DB::table('categories')->insert([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'game_id' => $category->gameId,
            'name' => $name,
            'slug' => Str::slug($name),
            'attributes' => Json::encode($category->attributes),
            'yaml_data' => $category->yamlData,
        ]);
    }

    /**
     * @param  string[]  $attributeNames
     * @param  Scores  $scores
     * @return \stdClass[]
     */
    private function readCategories(
        array $attributeNames,
        int $gameId,
        array $scores
    ): array {
        $combinations = $this->findAttributeCombinations(
            $attributeNames,
            $scores
        );

        $categories = [];

        foreach ($combinations as $combination) {
            $usedScores = [];

            foreach ($combination->scoreIndexes as $scoreIndex) {
                $score = $scores[$scoreIndex];

                // Attributes are stored on category level, so we can remove
                // them from the scores.
                foreach ($combination->attributes as $attrName => $attrValue) {
                    // Sanity check.
                    if ($score[$attrName] !== $attrValue) {
                        throw new \UnexpectedValueException(
                            "Score with index `{$scoreIndex}` should have an attribute"
                                ." named `{$attrName}` set to `{$attrValue}`"
                        );
                    }

                    unset($score[$attrName]);
                }

                $usedScores[] = $score;
                unset($scores[$scoreIndex]);
            }

            $categories[] = $this->createCategory(
                $gameId,
                $combination->attributes,
                $usedScores
            );
        }

        if ($scores !== []) {
            throw new \UnexpectedValueException(
                'All scores should have been used in a category'
            );
        }

        return $categories;
    }

    /**
     * @param  array<string,mixed>  $attributes
     * @param  Scores  $scores
     */
    private function createCategory(
        int $gameId,
        array $attributes,
        array $scores
    ): \stdClass {
        $category = new \stdClass();
        $category->gameId = $gameId;
        $category->attributes = $attributes;
        $category->yamlData = Yaml::dump([
            'scores' => $scores,
        ]);

        return $category;
    }

    /**
     * @param  string[]  $attributeNames
     * @param  Scores  $scores
     * @return \stdClass[]
     */
    private function findAttributeCombinations(
        array $attributeNames,
        array $scores
    ): array {
        $combinations = [];

        foreach ($scores as $scoreIndex => $score) {
            $attributes = [];

            foreach ($attributeNames as $attrName) {
                $attrValue = $score[$attrName] ?? null;

                if ($attrValue !== null) {
                    $attributes[$attrName] = $attrValue;
                }
            }

            $key = md5(Json::encode($attributes));

            if (! isset($combinations[$key])) {
                $combination = new \stdClass();
                $combination->attributes = $attributes;
                $combination->scoreIndexes = [];

                $combinations[$key] = $combination;
            }

            $combinations[$key]->scoreIndexes[] = $scoreIndex;
        }

        return array_values($combinations);
    }

    /**
     * @param  GlobalLayout  $layout
     * @return string[]
     */
    private function readAttributeNames(array $layout): array
    {
        return $layout['group']['scores'];
    }

    /**
     * @return GlobalLayout
     */
    private function readGlobalLayout(): array
    {
        $records = DB::table('global_data')
            ->where('data_key', '=', 'layout')
            ->pluck('data_value')
            ->all();

        $yaml = $records[0] ?? null;

        if (! is_string($yaml)) {
            throw new \UnexpectedValueException(
                'Global layout is expected to be a string'
            );
        }

        $layout = Yaml::parseAsMap($yaml);

        if (
            ! is_array($layout['group'])
            || ! is_array($layout['group']['scores'])
        ) {
            throw new \UnexpectedValueException(
                'Global layout is expected to contain groups for scores'
            );
        }

        return $layout;
    }

    /**
     * @param  GlobalLayout  $layout
     */
    private function updateGlobalLayout(array $layout): void
    {
        unset($layout['group']['scores']);

        DB::table('global_data')
            ->where('data_key', '=', 'layout')
            ->update([
                'data_value' => Yaml::dump($layout),
            ]);
    }

    /**
     * @return array<int,string>
     */
    private function readGames(): array
    {
        $records = DB::table('games')
            ->pluck('yaml_data', 'id')
            ->all();

        $games = [];

        foreach ($records as $gameId => $gameYaml) {
            $games[$gameId] = $gameYaml;
        }

        return $games;
    }

    /**
     * @return GameYaml
     */
    private function parseGameYaml(string $yaml): array
    {
        $data = Yaml::parseAsMap($yaml);

        if (! is_array($data['scores'])) {
            throw new \UnexpectedValueException(
                'Scores are expected to be an array'
            );
        }

        return $data;
    }

    /**
     * @param  GameYaml  $yamlData
     */
    private function updateGameYaml(int $gameId, array $yamlData): void
    {
        unset($yamlData['scores']);

        DB::table('games')
            ->where('id', $gameId)
            ->update([
                'yaml_data' => Yaml::dump($yamlData),
            ]);
    }
}

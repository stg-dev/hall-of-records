<?php

namespace Database\Seeders;

use App\Import\Properties;
use App\Import\Yaml;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * @phpstan-type Score array<string,mixed>
 * @phpstan-type CategoryYaml array{
 *     scores: Score[]
 * }
 */
class ScoreSeeder extends Seeder
{
    public function run(): void
    {
        foreach ($this->readGameIds() as $gameId) {
            foreach ($this->readCategories($gameId) as $categoryId => $categoryYaml) {
                $yamlData = $this->parseCategoryYaml($categoryYaml);

                $scores = $this->readScores(
                    $gameId,
                    $categoryId,
                    $yamlData['scores']
                );

                $this->insertScores($scores);

                $this->updateCategoryYaml($categoryId, $yamlData);
            }
        }
    }

    /**
     * @param  \stdClass[]  $scores
     */
    private function insertScores(array $scores): void
    {
        foreach ($scores as $score) {
            $this->insertScore($score);
        }
    }

    private function insertScore(\stdClass $score): void
    {
        DB::table('scores')->insert([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'game_id' => $score->gameId,
            'category_id' => $score->categoryId,
            'player_id' => null,
            'entry_name' => $score->playerName,
            'score' => $score->score,
            'yaml_data' => $score->yamlData,
        ]);
    }

    /**
     * @param  Score[]  $scores
     * @return \stdClass[]
     */
    private function readScores(
        int $gameId,
        int $categoryId,
        array $scores
    ): array {
        return array_map(
            fn ($input) => $this->createScore(
                $gameId,
                $categoryId,
                new Properties($input)
            ),
            array_values(array_filter(
                $scores,
                fn ($score) => $this->isImportableScore($score)
            ))
        );
    }

    private function createScore(
        int $gameId,
        int $categoryId,
        Properties $input
    ): \stdClass {
        $score = new \stdClass();
        $score->gameId = $gameId;
        $score->categoryId = $categoryId;
        $score->playerName = $input->fetchStringOrNull('player');
        $score->score = $input->fetchString('score');
        $score->yamlData = $input->remainingAsYaml();

        return $score;
    }

    /**
     * @param  Score  $score
     */
    private function isImportableScore(array $score): bool
    {
        return $score['score'] !== null;
    }

    /**
     * @return int[]
     */
    private function readGameIds(): array
    {
        $gameIds = DB::table('games')
            ->pluck('id')
            ->all();

        /** @var int[] */
        return $gameIds;
    }

    /**
     * @return array<int,string>
     */
    private function readCategories(int $gameId): array
    {
        $records = DB::table('categories')
            ->where('game_id', $gameId)
            ->pluck('yaml_data', 'id')
            ->all();

        $categories = [];

        foreach ($records as $categoryId => $categoryYaml) {
            $categories[$categoryId] = $categoryYaml;
        }

        return $categories;
    }

    /**
     * @return CategoryYaml
     */
    private function parseCategoryYaml(string $yaml): array
    {
        $data = Yaml::parseAsMap($yaml);

        if (! is_array($data['scores'])) {
            throw new \UnexpectedValueException(
                'Scores are expected to be an array'
            );
        }

        return $data;
    }

    /**
     * @param  CategoryYaml  $yamlData
     */
    private function updateCategoryYaml(int $categoryId, array $yamlData): void
    {
        $remainingScores = array_values(array_filter(
            $yamlData['scores'],
            fn ($score) => ! $this->isImportableScore($score)
        ));

        $yamlData['scores'] = array_values($remainingScores);

        if ($yamlData['scores'] === []) {
            unset($yamlData['scores']);
        }

        DB::table('categories')
            ->where('id', $categoryId)
            ->update([
                'yaml_data' => Yaml::dump($yamlData),
            ]);
    }
}

<?php

namespace Database\Seeders;

use App\Import\Properties;
use App\Import\Translation\CompanyNameTransliterator;
use App\Import\Translation\Locale;
use App\Import\Translation\Translations;
use App\Import\Yaml;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * @phpstan-type GlobalTranslation array<string,mixed>
 */
class CompanySeeder extends Seeder
{
    public function run(): void
    {
        $translations = $this->readGlobalTranslations();

        $companies = $this->readCompanies($translations);

        $this->insertCompanies($companies);

        $this->updateGlobalTranslations($translations);
    }

    /**
     * @param  \stdClass[]  $companies
     */
    private function insertCompanies(array $companies): void
    {
        foreach ($companies as $index => $company) {
            $company->id = $index + 1;

            $this->insertCompany($company);
            $this->insertCompanyTranslations($company, Locale::En);
            $this->insertCompanyTranslations($company, Locale::Ja);
        }
    }

    private function insertCompany(\stdClass $company): void
    {
        DB::table('companies')->insert([
            'id' => $company->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'name_search' => $company->names->implode(),
            'yaml_data' => $company->yamlData,
        ]);
    }

    private function insertCompanyTranslations(
        \stdClass $company,
        Locale $locale
    ): void {
        DB::table('company_translations')->insert([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'company_id' => $company->id,
            'locale' => $locale->value,
            'name' => $company->names->get($locale),
            'name_translit' => $company->translitNames->get($locale),
        ]);
    }

    /**
     * @param  GlobalTranslation[]  $translations
     * @return \stdClass[]
     */
    private function readCompanies(array $translations): array
    {
        return array_map(
            fn ($input) => $this->createCompany(
                new Properties($input)
            ),
            array_values(array_filter(
                $translations,
                fn ($translation) => $this->isCompanyTranslations($translation)
            ))
        );
    }

    private function createCompany(Properties $input): \stdClass
    {
        $transliterator = new CompanyNameTransliterator();

        $company = new \stdClass();
        $company->names = Translations::required()
            ->add(Locale::En, $input->fetchString('value'))
            ->add(Locale::Ja, $input->fetchString('value-jp'));

        $company->translitNames = $transliterator->transliterate($company->names);
        $company->yamlData = $input->remainingAsYaml();

        return $company;
    }

    /**
     * @param  GlobalTranslation  $translation
     */
    private function isCompanyTranslations(array $translation): bool
    {
        return $translation['property'] === 'company';
    }

    /**
     * @return GlobalTranslation[]
     */
    private function readGlobalTranslations(): array
    {
        $records = DB::table('global_data')
            ->where('data_key', '=', 'translations')
            ->pluck('data_value')
            ->all();

        $translations = $records[0] ?? null;

        if (! is_string($translations)) {
            throw new \UnexpectedValueException(
                'Global translations are expected to be a string'
            );
        }

        return Yaml::parseAsList($translations);
    }

    /**
     * @param  GlobalTranslation[]  $translations
     */
    private function updateGlobalTranslations(array $translations): void
    {
        $remainingTranslations = array_values(array_filter(
            $translations,
            fn ($translation) => ! $this->isCompanyTranslations($translation)
        ));

        DB::table('global_data')
            ->where('data_key', '=', 'translations')
            ->update([
                'data_value' => Yaml::dump($remainingTranslations),
            ]);
    }
}

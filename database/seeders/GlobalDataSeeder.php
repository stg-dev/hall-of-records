<?php

namespace Database\Seeders;

use App\Import\Yaml;
use App\Import\YamlDatabase;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * @phpstan-import-type GlobalData from YamlDatabase
 */
class GlobalDataSeeder extends Seeder
{
    public function run(): void
    {
        $yamlDb = YamlDatabase::load();

        $this->insertGlobalData($yamlDb->global);
    }

    /**
     * @param  GlobalData  $global
     */
    private function insertGlobalData(array $global): void
    {
        foreach ($global as $key => $value) {
            DB::table('global_data')->insert([
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'data_key' => $key,
                'data_value' => Yaml::dump($value),
            ]);
        }
    }
}

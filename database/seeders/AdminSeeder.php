<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    public function run(): void
    {
        if (User::query()->where('email', 'admin@example.com')->first() !== null) {
            return;
        }

        User::factory([
            'email' => 'admin@example.com',
            'password' => Hash::make('password'),
        ])->create();
    }
}

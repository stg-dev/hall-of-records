<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call([
            AdminSeeder::class,
            GlobalDataSeeder::class,
            CompanySeeder::class,
            GameSeeder::class,
            CategorySeeder::class,
            ScoreSeeder::class,
            PlayerSeeder::class,
        ]);
    }
}

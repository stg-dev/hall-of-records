<?php

namespace Database\Seeders;

use App\Import\Properties;
use App\Import\Translation\Locale;
use App\Import\Translation\Translations;
use App\Import\YamlDatabase;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * @phpstan-import-type Games from YamlDatabase
 */
class GameSeeder extends Seeder
{
    public function run(): void
    {
        $yamlDb = YamlDatabase::load();

        $games = $this->readGames($yamlDb->games);

        $this->insertGames($games);
    }

    /**
     * @param  \stdClass[]  $games
     */
    private function insertGames(array $games): void
    {
        foreach ($games as $index => $game) {
            $game->id = $index + 1;

            $this->insertGame($game);
            $this->insertGameTranslations($game, Locale::En);
            $this->insertGameTranslations($game, Locale::Ja);
            $this->insertGameCompanies($game);
        }
    }

    private function insertGame(\stdClass $game): void
    {
        $names = $game->names->implode();

        DB::table('games')->insert([
            'id' => $game->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'slug' => Str::slug($game->names->get(Locale::En)),
            'name_search' => $names,
            'yaml_data' => $game->yamlData,
        ]);
    }

    private function insertGameTranslations(
        \stdClass $game,
        Locale $locale
    ): void {
        DB::table('game_translations')->insert([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'game_id' => $game->id,
            'locale' => $locale->value,
            'name' => $game->names->get($locale),
            'name_translit' => $game->translitNames->get($locale),
        ]);
    }

    private function insertGameCompanies(\stdClass $game): void
    {
        foreach ($game->companyIds as $companyId) {
            DB::table('company_game')->insert([
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'game_id' => $game->id,
                'company_id' => $companyId,
            ]);
        }
    }

    /**
     * @param  Games  $games
     * @return \stdClass[]
     */
    private function readGames(array $games): array
    {
        return array_map(
            fn ($input) => $this->createGame(
                new Properties($input)
            ),
            $games
        );
    }

    private function createGame(Properties $input): \stdClass
    {
        $game = new \stdClass();
        $game->companyIds = $this->findCompanyIds(
            $input->fetchString('company')
        );
        $game->names = Translations::required()
            ->add(Locale::En, $input->fetchString('name'))
            ->add(Locale::Ja, $input->fetchString('name-jp'));
        $game->translitNames = Translations::optional()
            ->add(Locale::En, null)
            ->add(Locale::Ja, $input->fetchString('name-kana'));

        $game->yamlData = $input->remainingAsYaml();

        return $game;
    }

    /**
     * @return int[]
     */
    private function findCompanyIds(string $companyName): array
    {
        try {
            return [
                $this->findCompanyId($companyName),
            ];
        } catch (\UnexpectedValueException $exception) {
            // If multiple companies are specified, we try to find each of them.
            if (! str_contains($companyName, '/')) {
                throw $exception;
            }

            return array_map(
                fn ($name) => $this->findCompanyId(trim($name)),
                explode('/', $companyName)
            );
        }
    }

    private function findCompanyId(string $companyName): int
    {
        $records = DB::table('company_translations')
            ->where('name', '=', $companyName)
            ->where('locale', '=', Locale::En->value)
            ->pluck('company_id')
            ->all();

        $companyId = $records[0] ?? null;

        if (! is_int($companyId)) {
            throw new \UnexpectedValueException(
                "Unable to find company id for name `{$companyName}`"
            );
        }

        return $companyId;
    }
}

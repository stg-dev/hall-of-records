<?php

namespace Database\Seeders;

use App\Import\Yaml;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * @phpstan-type ScoreYaml array{
 *     player-id: string
 * }
 */
class PlayerSeeder extends Seeder
{
    public function run(): void
    {
        $players = [];

        foreach ($this->readScoresWithLinkedPlayer() as $scoreId => $scoreYaml) {
            $yamlData = $this->parseScoreYaml($scoreYaml);

            $playerName = $yamlData['player-id'];

            if (! isset($players[$playerName])) {
                $players[$playerName] = $this->insertPlayer($playerName);
            }

            $this->updateScorePlayer($scoreId, $players[$playerName]);

            $this->updateScoreYaml($scoreId, $yamlData);
        }

        foreach ($this->readScoresWithoutLinkedPlayer() as $scoreId => $playerName) {
            if (! isset($players[$playerName])) {
                $players[$playerName] = $this->insertPlayer($playerName);
            }

            $this->updateScorePlayer($scoreId, $players[$playerName]);
        }
    }

    private function insertPlayer(string $name): int
    {
        return DB::table('players')->insertGetId([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'name' => $name,
        ]);
    }

    /**
     * @return array<int,string>
     */
    private function readScoresWithLinkedPlayer(): array
    {
        $records = DB::table('scores')
            ->where('yaml_data', 'like', '%player-id:%')
            ->pluck('yaml_data', 'id')
            ->all();

        $scores = [];

        foreach ($records as $scoreId => $scoreYaml) {
            $scores[$scoreId] = $scoreYaml;
        }

        return $scores;
    }

    /**
     * @return array<int,string>
     */
    private function readScoresWithoutLinkedPlayer(): array
    {
        $records = DB::table('scores')
            ->where('player_id', '=', null)
            ->where('entry_name', '!=', null)
            ->pluck('entry_name', 'id')
            ->all();

        $scores = [];

        foreach ($records as $scoreId => $playerName) {
            $scores[$scoreId] = $playerName;
        }

        return $scores;
    }

    private function updateScorePlayer(int $scoreId, int $playerId): void
    {
        DB::table('scores')
            ->where('id', $scoreId)
            ->update([
                'player_id' => $playerId,
            ]);
    }

    /**
     * @return ScoreYaml
     */
    private function parseScoreYaml(string $yaml): array
    {
        $data = Yaml::parseAsMap($yaml);

        if (! is_string($data['player-id'])) {
            throw new \UnexpectedValueException(
                'Player id is expected to be a string'
            );
        }

        return $data;
    }

    /**
     * @param  ScoreYaml  $yamlData
     */
    private function updateScoreYaml(int $scoreId, array $yamlData): void
    {
        unset($yamlData['player-id']);

        DB::table('scores')
            ->where('id', $scoreId)
            ->update([
                'yaml_data' => Yaml::dump($yamlData),
            ]);
    }
}

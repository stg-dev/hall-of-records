-
    name: global
    description: "[[File:Micomlogo.jpg|800px|center]]\n\n\nWelcome to the STG Hall of Records!\n\nIn the following page you will find a list of all world record scores for arcade shmups. The list is a major revision of the [https://shmups.system11.org/viewtopic.php?f=2&t=38524 long-standing topic on the Shmups Forum] under the same name, which was maintained by the user NTSC-J from September 2011 to July 2020. This list takes the Japanese score keeping traditions as a basis, primarily the uninterrupted combined tradition of Gamest magazine (1986 to 1999), Arcadia magazine (1999 to 2015) and the [http://www.jha-arcade.com website] of the Japan Highscore Association a.k.a. JHA (2016 to the present). For the current revision of the STG Hall of Records, several more scores have been retrieved from other sources such as Micom Basic magazine (1984 to 1999) but also private Twitter accounts, forums, and other websites. To read more about the tracking of arcade highscores in Japan, please check [http://electricunderground.io/guest-article-introduction-to-scorekeeping-for-japanese-arcade-games/ this short article].\n[[File:MICB.jpg|400px|right]]\nThe STG Hall of Records in its current form would have been an unthinkable project without the meticulous work carried out by Marco “Gemant” Frattino who has been spreading information about Japanese arcade highscores with tireless enthusiasm within the english-speaking community for over a decade. Gemant’s work culminates in the latest publication of his monumental project ARCA (started in 2007), a document providing information on a selection of 600 arcade games (not only shmups), which can be [https://www.mameretroavengers.com/documenti/arca-general-dei-record-arcade/ accessed here]. However, the STG Hall of Records is not merely a copy of Gemant’s document. Whenever possible, the records have been double checked and verified. Furthermore, you will notice two fundamental departures from the ARCA project. First, while non-shmups have been excluded, the STG Hall of Records aims to cover every arcade shmup and is constantly growing, not limited to a specific number of games. Second, games from the so-called “Golden Era” (pre 1985 and therefore prior to the first issue of Gamest) are also considered.\n\nAs an additional innovation, the STG Hall of Records also lists scores for various categories that otherwise do not exist within the Japanese tradition, e.g. a more in-depth and specific recordkeeping of subtypes of ships (e.g. including shot and laser types in [[DoDonPachi]]). Scores from particularly accurate ports (e.g. [[Battle Garegga]] on PS4 or [[Crimzon Clover]] on Steam) or scores achieved on emulators (e.g. Mame or GOTVG) can also be found on the list. These scores are not meant to replace the current arcade scores and are but seen as a supplement. Geographically speaking, scores from all over the world are considered.\n\nMostly limited to a number of older games, Japanese scorekeeping sometimes sets a goal score of 10,000,000 points and closes the game for competition once this threshold is achieved regardless of whether the score counter would continue normally or roll over back to 0. These scores are marked by the notation of \"+α\", e.g. \"10,000,000+α\" and are then marked in green color to indicate that the ranking is officially closed under JHA rules. The STG Hall of Records likewise lists these +α-scores and indicates the first player(s) to reach this score (players achieving the same goal score later on are not considered). Moreover, the highest score beyond this arbitrary threshold is also listed. Only when a true counterstop is achieved, the game or category will be closed for competition as long as no special rules apply as is the case with Ultra Mode in [[Mushihimesama Futari]] 1.5. In the case of a counterstop, the ranking will be marked in grey color to indicate that it is closed.\n"
    layout:
        sort:
            games:
                name: asc
            scores:
                score: desc
        group:
            scores:
                - version
                - autofire
                - platform
                - game
                - mode
                - difficulty
                - ship
                - weapon
                - loop
        column-order:
            - version
            - autofire
            - game
            - mode
            - difficulty
            - ship
            - weapon
            - loop
            - score
            - player
            - sources
            - comments
        columns:
            player:
                label: Player
                label-jp: プレイヤー
                template: '{{ score.player }}'
            score:
                label: Score
                label-jp: スコア
                template: "{{ score.score }}{% if attribute(score, 'score-real') and attribute(score, 'score-real') != score.score %} [{{ attribute(score, 'score-real') }}]{% endif %}\n"
            ship:
                label: Ship
                label-jp: 自機
                template: '{{ score.ship }}'
                attributes:
                    is-category: true
                    is-mergeable: true
            game:
                label: Game
                label-jp: ゲーム
                template: '{{ score.game }}'
                attributes:
                    is-category: true
                    is-mergeable: true
            mode:
                label: Mode
                label-jp: モード
                template: '{{ score.mode }}'
                attributes:
                    is-category: true
                    is-mergeable: true
            difficulty:
                label: Difficulty
                label-jp: 難易度
                template: '{{ score.difficulty }}'
                attributes:
                    is-category: true
                    is-mergeable: true
            loop:
                label: Loop
                label-jp: 2周種
                template: '{{ score.loop }}'
                attributes:
                    is-category: true
                    is-mergeable: true
            weapon:
                label: Weapon
                label-jp: ショット
                template: '{{ score.weapon }}'
                attributes:
                    is-category: true
                    is-mergeable: true
            comments:
                label: Comment
                label-jp: 備考
                template: "{{ score.comments|join(', ') }}"
            version:
                label: Version
                label-jp: バージョン
                template: '{{ score.version }}'
                attributes:
                    is-category: true
                    is-mergeable: true
            autofire:
                label: Autofire
                label-jp: オート連射
                template: '{{ score.autofire }}'
                attributes:
                    is-category: true
                    is-mergeable: true
            platform:
                label: Platform
                label-jp: プラットフォーム
                template: '{{ score.platform }}'
            sources:
                label: Source
                label-jp: 情報元
                template: "{% for source in score.sources %}{% if source.url %}[{{ source.url }} {{ source.name }}]{% else %}{{ source.name|formatDate }}{% endif %}{% if source.date %} ({{ source.date|formatDate }}){% endif %}{% if not loop.last %}<br>{% endif %}{% endfor %}\n"
        templates:
            main: "{{ description|raw }}\n{{ include('toc') }}\n{{ include('games') }}\n"
            toc: "<table class=\"wikitable\">\n  <tr>\n{% set numNonEmptyGroups = 0 %}\n{% for group in games.grouped.byInitials %}\n{% if group.games %}\n    <th class=\"{% for game in group.games %}mw-customtoggle-{{ game.properties.id }} {% endfor %}\" style=\"padding-left:10px;padding-right:10px;\">{{ group.title }}</th>\n{% set numNonEmptyGroups = numNonEmptyGroups + 1 %}\n{% endif %}\n{% endfor %}\n  </tr>\n{% for group in games.grouped.byInitials %}\n{% for game in group.games %}\n  <tr id=\"mw-customcollapsible-{{ game.properties.id }}\" class=\"mw-collapsible mw-collapsed\">\n    <td colspan=\"{{ numNonEmptyGroups }}\">[[#{{ game.properties.name }}|{{ game.properties.name }}]]</td>\n  </tr>\n{% endfor %}\n{% endfor %}\n</table>\n__NOTOC__\n"
            games: "{% for game in games.all %}\n{# Use custom game template where available. #}\n{% set customTemplate = \"game-#{game.properties.id}\" %}\n{{ include([customTemplate, 'game']) }}\n{% endfor %}\n"
            game: "{# Always us an array for counterstops #}\n{% set counterstops = [] %}\n{% if game.properties.counterstop.score %}{# Single counterstop defined #}\n{% set counterstops = [game.properties.counterstop] %}\n{% elseif game.properties.counterstop %}{# Multiple counterstops defined #}\n{% set counterstops = game.properties.counterstop %}\n{% endif %}\n{{ '{{' }}Anchor|{{ game.properties.name }}{{ '}}' }}\n{| class=\"wikitable\" style=\"text-align: center\"\n|-\n! colspan=\"{{ game.headers|length }}\" | {{ game.properties.name }} ({{ game.properties.company }})\n{# Current records #}\n{% set scores = [] %}\n{% for score in game.scores %}\n{% if score.properties['is-current-record'] %}\n{% set scores = scores|merge([score]) %}\n{% endif %}\n{% endfor %}\n|-\n! {{ game.headers|join(' !! ') }}\n{% for scoreIndex,score in scores %}\n{{ include('score') }}\n{% endfor %}\n{# Previous records #}\n{% set scores = [] %}\n{% for score in game.scores %}\n{% if not score.properties['is-current-record'] %}\n{% set scores = scores|merge([score]) %}\n{% endif %}\n{% endfor %}\n{# Set guard to false to prevent previous records from being included in the output. #}\n{% if true and scores %}\n{% set gameId = game.properties.name|md5 %}\n{% set scoreCssAttrs = {\n    'id': \"mw-customcollapsible-#{gameId}\",\n    'class': 'mw-collapsible mw-collapsed',\n    'style': \"background-color:#eee; color:#999;\"\n} %}\n<!-- Remove table rows below if you do NOT want previous records to be displayed. -->\n\n|- id=\"mw-customcollapsible-{{ gameId }}\" class=\"mw-customtoggle-{{ gameId }} mw-collapsible\"\n| colspan=\"{{ game.headers|length }}\" style=\"{{ scoreCssAttrs['style'] }} font-size: smaller; padding: 2px;\" | {% if locale == 'jp' %}過去の記録を表示する{% else %}Show previous records{% endif %} \n|- id=\"mw-customcollapsible-{{ gameId }}\" class=\"mw-customtoggle-{{ gameId }} mw-collapsible mw-collapsed\"\n| colspan=\"{{ game.headers|length }}\" style=\"{{ scoreCssAttrs['style'] }} font-size: smaller; padding: 2px;\" | {% if locale == 'jp' %}過去の記録を隠す{% else %}Hide previous records{% endif %} \n|- {% for name,value in scoreCssAttrs %}{{ name }}=\"{{ value }}\" {% endfor %} \n! {{ game.headers|join(' !! ') }}\n{% for scoreIndex,score in scores %}\n{{ include('score') }}\n{% endfor %}\n{% endif %}\n|}\n\n{% if game.properties.description %}\n{{ game.properties.description }}\n\n{% endif %}\n{% if game.properties.links %}\n{% for link in game.properties.links %}\n* [{{ link.url}} {{link.title}}]\n{% endfor %}\n{% endif %}\n"
            score: "{% set prevScore = scores[scoreIndex - 1] %}\n{% set nextScore = scores[scoreIndex + 1] %}\n{# Check for counterstops #}\n{% set isCounterstop = false %}\n{% set counterstopType = null %}\n{% for counterstop in counterstops %}\n{% if counterstop.score and score.properties.score == counterstop.score %}\n{% set isCounterstop = true %}\n{% set counterstopType = counterstop.type %}\n{% endif %}\n{% endfor %}\n|- {% for name,value in scoreCssAttrs %}{{ name }}=\"{{ value }}\" {% endfor %} \n| {% for columnIndex,column in score.columns %}\n{% set cellValue = column.value %}\n{% set classes = '' %}\n{% if isCounterstop and not column.attributes['is-category'] %}\n{% set classes = \"hor-counterstop-#{counterstopType} #{classes}\" %}\n{% endif %}\n{# Merge counterstop score values as well #}\n{% if column.attributes['is-mergeable']\n   or column.name == 'score'\n   and isCounterstop\n   and score.properties.score == score.properties['score-real'] %}\n{% set mergeWithTopCell = true %}\n{% set mergeWithBottomCell = true %}\n{% for i in 0..columnIndex %}\n{% if not prevScore or prevScore.columns[i].value != score.columns[i].value %}\n{% set mergeWithTopCell = false %}\n{% endif %}\n{% if not nextScore or nextScore.columns[i].value != score.columns[i].value %}\n{% set mergeWithBottomCell = false %}\n{% endif %}\n{% endfor %}\n{% if mergeWithTopCell and mergeWithBottomCell %}\n{% set cellValue = '' %}\n{% set classes = \"hor-merged-cell #{classes}\" %}\n{% elseif mergeWithTopCell %}\n{% set cellValue = '' %}\n{% set classes = \"hor-merged-cell-last #{classes}\" %}\n{% elseif mergeWithBottomCell %}\n{% set classes = \"hor-merged-cell-first #{classes}\" %}\n{% endif %}\n{% endif %}\n{% if classes %}class=\"{{ classes }}\" | {% endif %}\n{{ cellValue|raw }}{% if not loop.last %} || {% endif %}\n{% endfor %}\n"
    translations:
        -
            property: company
            value: ADK
            value-jp: エーディーケイ
        -
            property: company
            value: Afega
            value-jp: アフェーガ
        -
            property: company
            value: A.I.
            value-jp: エーアイ
        -
            property: company
            value: 'Aicom / Yumekobo'
            value-jp: 'エイコム / 夢工房'
        -
            property: company
            value: 'Alfa System'
            value-jp: アルファ・システム
        -
            property: company
            value: Allumer
            value-jp: アルュメ
        -
            property: company
            value: Athena
            value-jp: アテナ
        -
            property: company
            value: Atlus
            value-jp: アトラス
        -
            property: company
            value: Banpresto
            value-jp: バンプレスト
        -
            property: company
            value: Capcom
            value-jp: カプコン
        -
            property: company
            value: CAVE
            value-jp: ケイブ
        -
            property: company
            value: Coreland
            value-jp: コアランド
        -
            property: company
            value: Crux
            value-jp: クラックス
        -
            property: company
            value: 'Data East'
            value-jp: データイースト
        -
            property: company
            value: Dooyong
            value-jp: ドーヤン
        -
            property: company
            value: 'East Technology'
            value-jp: イーストテクノロジー
        -
            property: company
            value: exA-Arcadia
            value-jp: exA-Arcadia
        -
            property: company
            value: Face
            value-jp: フェイス
        -
            property: company
            value: Gazelle
            value-jp: ガゼル
        -
            property: company
            value: G.Rev
            value-jp: グレフ
        -
            property: company
            value: Hudson
            value-jp: ハドソン
        -
            property: company
            value: IGS
            value-jp: IGS
        -
            property: company
            value: Irem
            value-jp: アイレム
        -
            property: company
            value: Jaleco
            value-jp: ジャレコ
        -
            property: company
            value: Jorudan
            value-jp: ジョルダン
        -
            property: company
            value: Kaneko
            value-jp: カネコ
        -
            property: company
            value: Konami
            value-jp: コナミ
        -
            property: company
            value: M2
            value-jp: エムツー
        -
            property: company
            value: Mebius
            value-jp: メビウス
        -
            property: company
            value: Metro
            value-jp: メトロ
        -
            property: company
            value: Milestone
            value-jp: マイルストーン
        -
            property: company
            value: Mitchell
            value-jp: ミッチェル
        -
            property: company
            value: Namco
            value-jp: ナムコ
        -
            property: company
            value: Nichibutsu
            value-jp: ニチブツ
        -
            property: company
            value: 'Nihon System'
            value-jp: 日本システム
        -
            property: company
            value: NMK
            value-jp: NMK
        -
            property: company
            value: Psikyo
            value-jp: 彩京
        -
            property: company
            value: 'Raizing / 8ing'
            value-jp: 'ライジング / エイティング'
        -
            property: company
            value: Sammy
            value-jp: サミー
        -
            property: company
            value: Sega
            value-jp: セガ
        -
            property: company
            value: 'Seibu Kaihatsu / Moss'
            value-jp: 'セイブ開発 / モス'
        -
            property: company
            value: Seta
            value-jp: セタ
        -
            property: company
            value: Skonec
            value-jp: スコーネック
        -
            property: company
            value: SNK
            value-jp: SNK
        -
            property: company
            value: 'Studio Siesta'
            value-jp: スタジオシエスタ
        -
            property: company
            value: Success
            value-jp: サクセス
        -
            property: company
            value: Taito
            value-jp: タイトー
        -
            property: company
            value: Takumi
            value-jp: タクミ
        -
            property: company
            value: Tanoshimasu
            value-jp: タノシマス
        -
            property: company
            value: Tecmo
            value-jp: テクモ
        -
            property: company
            value: Tecnosoft
            value-jp: テクノソフト
        -
            property: company
            value: Tehkan
            value-jp: テーカン
        -
            property: company
            value: Toaplan
            value-jp: 東亜プラン
        -
            property: company
            value: Treasure
            value-jp: トレジャー
        -
            property: company
            value: 'Triangle Service / Oriental Soft'
            value-jp: 'トライアングル・サービス / オリエンタルソフト'
        -
            property: company
            value: UPL
            value-jp: UPL
        -
            property: company
            value: 'Video System'
            value-jp: ビデオシステム
        -
            property: company
            value: Visco
            value-jp: ビスコ
        -
            property: company
            value: Warashi
            value-jp: 童
        -
            property: company
            value: 'Yang Chen'
            value-jp: ヤンチェン
        -
            property: company
            value: Yotsubane
            value-jp: 四ツ羽根
        -
            property: company
            value: DORAGON
            value-jp: ドラゴン
        -
            property: company
            value: RIKI
            value-jp: RIKI
        -
            property: company
            value: Touhou FDF
            value-jp: 東方FDF
        -
            property: company
            value: Neotro
            value-jp: ネオトロ
        -
            property: sources
            value: Arcadia
            value-jp: アルカディア
            fuzzy-match: true
        -
            property: sources
            value: Gamest
            value-jp: ゲーメスト
            fuzzy-match: true
        -
            property: sources
            value: 'Micom Basic'
            value-jp: マイコンBASIC
            fuzzy-match: true
        -
            property: sources
            value: Twitter
            value-jp: ツイッター
            fuzzy-match: true
        -
            property: sources
            value: niconico
            value-jp: ニコニコ
            fuzzy-match: true
        -
            property: links
            value: 'JHA Leaderboard'
            value-jp: 日本ハイスコア協会
            fuzzy-match: true

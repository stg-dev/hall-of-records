'''Attention: This page serves as a database and is not meant to be consumed by end users. If you came here for the STG Hall of Records, follow one of these links:'''

* English version: https://shmups.wiki/library/STG_Hall_of_Records
* 日本語版: https://shmups.wiki/library/STG記録の殿堂

== Games ==

=== Last Duel ===
<pre><nowiki>
name: "Last Duel"
name-jp: "ラストデュエル"
name-kana: "らすとでゅえる"
company: "Capcom"
needs-work: true

scores:
  - score: 806,400
    player: DLF-ZTT-M.M
    sources:
      - name: Gamest
        date: "1988-09"

  - score: 811,050
    player: 説教ZAC-Ristar お店に感謝(2軒)
    sources:
      - name: Gamest
        date: "1999-05"

  - score: 823,450
    player: hamam!
    sources:
      - name: Arcadia
        date: "2014-09"
    comments:
      - "autofire"
      - "9L remaining"

  - score: 829,600
    player: hamami
    sources:
      - name: Arcadia
        date: "2014-12"
    comments:
      - "autofire"
      - "9L remaining"

links:
  - url: https://shmups.system11.org/viewtopic.php?t=29454
    title: Shmups Forum Hi-Score Topic
</nowiki></pre>

=== Last Resort ===
<pre><nowiki>
name: "Last Resort"
name-jp: "ラストリゾート"
name-kana: "らすとりぞーと"
company: "SNK"
needs-work: true

scores:
  - score: 737,100
    player: ノーベル期待の新星! Wブレードやりたい君
    autofire: -
    sources:
      - name: Gamest
        date: "1992-06"
    comments:
      - "2 miss"
    attributes:
        is-current-record: false

  - score: 802,100
    player: CYR-PET
    autofire: OFF
    sources:
      - name: Gamest
        date: "1992-08"

  - score: 818,900
    player: MOL
    autofire: OFF
    sources:
      - name: Gamest
        date: "1992-10"

  - score: 829,700
    player: MOL (さえない軍団)
    autofire: OFF
    sources:
      - name: Gamest
        date: "1992-11"

  - score: 832,900
    player: MOL
    autofire: OFF
    sources:
      - name: Gamest
        date: "1992-12"

  - score: 838,700
    player: 手の早さはブラゴンの隣りのおっさん並 山本(醜)	
    autofire: OFF
    sources:
      - name: Arcadia
        date: "2004-10"

  - score: 832,300
    player: 世紀末覇者味噌汁王HYAKU-UGF
    autofire: ON
    sources:
      - name: Gamest
        date: "1992-07"

  - score: 859,000
    player: 4番装備はやんなくて結構、星白金-HIR
    autofire: ON
    sources:
      - name: Gamest
        date: "1992-08"

  - score: 869,800
    player: 醜!!男塾 山本
    autofire: ON
    sources:
      - name: Arcadia
        date: "2003-10"
    comments:
      - "1st loop 315k"
      - "0L remaining"

  - score: 873,300
    player: G.M.C..Z.
    autofire: ON
    sources:
      - name: Arcadia
        date: "2007-07"

  - score: 873,800
    player: G.M.C..Z.
    autofire: ON
    sources:
      - name: Arcadia
        date: "2007-10"

  - score: 891,200
    player: CBてんぷら
    autofire: ON
    sources:
      - name: JHA
        date: "2018-09"
    comments:
      - "1st loop 324k"

  - score: 905,800
    player: CBてんぷら
    autofire: ON
    sources:
      - name: JHA
        date: "2018-10"
    comments:
      - "1st loop 326k"

  - score: 912,300
    player: CBてんぷら
    autofire: ON
    sources:
      - name: JHA
        date: "2018-12"
    comments:
      - "1st loop 328k"

links:
  - url: https://wiki.denfaminicogamer.jp/highscore/1992_014_%E3%83%A9%E3%82%B9%E3%83%88%E3%83%AA%E3%82%BE%E3%83%BC%E3%83%88
    title: JHA Leaderboard
  - url: https://shmups.system11.org/viewtopic.php?t=59541
    title: Shmups Forum Hi-Score Topic

layout:
    sort:
        scores:
            autofire: asc
</nowiki></pre>

=== Life Force ===
<pre><nowiki>
name: "Life Force"
name-jp: "ライフフォース"
name-kana: "らいふふぉーす"
company: "Konami"
counterstop:
  - score: 10,000,000+α
    type: soft
needs-work: true

scores:
  - score: 4,204,500
    player: HTL-久保恵美子
    mode: 1P
    sources:
      - name: Gamest
        date: "1987-10"
    comments:
      - "stage 8-3"
    attributes:
        is-current-record: false

  - score: 6,800,000
    player: BROS-今枝だ!
    mode: 1P
    sources:
      - name: Gamest
        date: "1987-11"
    comments:
      - "loop 12"
      - "27 lives remaining"
    attributes:
        is-current-record: false

  - score: 10,000,000+α
    player: GMC-ZEUS
    mode: 1P
    sources:
      - name: Gamest
        date: "1987-12"
    comments:
      - "stage 17-4"
    attributes:
        is-current-record: true

  - score: 10,000,000+α
    player: ペガサスの常連H・H
    mode: 1P
    sources:
      - name: Micom Basic
        date: "1987-11"
    comments:
      - "stage 17-2"
    attributes:
        is-current-record: true

  - score: 10,000,000+α
    player: HTL-GMC-KOM
    mode: 1P
    sources:
      - name: Gamest
        date: "1987-12"
      - name: Micom Basic
        date: "1987-11"
    comments:
      - "non-default extend settings (5 lives)"
      - "stage 18-2"
      - "32 lives remaining"
    attributes:
        is-current-record: true

  - score: 10,000,000+α
    player: X.Y.Z.つかつか
    mode: 1P
    sources:
      - name: Micom Basic
        date: "1987-11"
    comments:
      - "stage 25-6"
    attributes:
        is-current-record: true

  - score: 5,868,000
    player: SPREAM-K.W
    mode: 2P
    sources:
      - name: Gamest
        date: "1987-09"
    comments:
      - "stage 11-5"
    attributes:
        is-current-record: false

  - score: 10,000,000+α
    player: ILG-K.M
    mode: 2P
    sources:
      - name: Gamest
        date: "1987-10"
    attributes:
        is-current-record: true

  - score: 10,000,000+α
    player: BROS-今枝だ!
    mode: 2P
    sources:
      - name: Gamest
        date: "1987-10"
    attributes:
        is-current-record: true

  - score: 10,000,000+α
    player: LUCKY 7½ TM NOW WIN!
    mode: 2P
    sources:
      - name: Gamest
        date: "1987-10"
    comments:
      - "stage 19-2"
    attributes:
        is-current-record: true

  - score: 10,000,000+α
    player: 大森
    mode: 2P
    sources:
      - name: Gamest
        date: "1987-10"
    comments:
      - "loop 19"
    attributes:
        is-current-record: true

  - score: 10,000,000+α
    player: GMC-HTL-SCR
    mode: 2P
    sources:
      - name: Gamest
        date: "1987-10"
    comments:
      - "stage 17-4"
    attributes:
        is-current-record: true

  - score: 10,000,000+α
    player: GMC-ZEUS
    mode: 2P
    sources:
      - name: Gamest
        date: "1987-10"
    comments:
      - "stage 17-1"
    attributes:
        is-current-record: true

  - score: 10,000,000+α
    player: BROS-今枝だ
    mode: 2P
    sources:
      - name: Gamest
        date: "1987-10"
    comments:
      - "stage 18-2"
    attributes:
        is-current-record: true

  - score: 10,000,000+α
    player: HTL-AE-SPY
    mode: 2P
    sources:
      - name: Gamest
        date: "1987-10"
    comments:
      - "stage 17-4"
      - "42L remaining"
    attributes:
        is-current-record: true

  - score: 10,000,000+α
    player: HTL-謎のボケ浪人(め)
    mode: 2P
    sources:
      - name: Gamest
        date: "1987-10"
    comments:
      - "stage 18-1"
      - "9L remaining"
    attributes:
        is-current-record: true

  - score: 30,111,100
    player: PEM
    mode: 2P
    sources:
      - name: Gamest
        date: "1992-09"
    comments:
      - "playtime ~14h"

description: "Note: Japanese scoreboard closed after the achievement of the target score of 10,000,000."

links:
  - url: https://shmups.system11.org/viewtopic.php?t=5877
    title: Shmups Forum Hi-Score Topic

layout:
    columns:
        mode:
            label: Player Side
            label-jp: 側

    sort:
        scores:
            mode: asc
</nowiki></pre>

=== Lost Worlds / Forgotten Worlds ===
<pre><nowiki>
name: "Lost Worlds / Forgotten Worlds"
name-jp: "ロストワールド"
name-kana: "ろすとわーるど"
company: "Capcom"
needs-work: true

scores:
  - score: 8,351,200
    player: HCS-KAW
    version: New
    mode: "[1 Player]"
    sources:
      - name: Gamest
        date: "1988-12"

  - score: 8,538,400
    player: HCS-KAW
    version: New
    mode: "[1 Player]"
    sources:
      - name: Gamest
        date: "1989-01"

  - score: 8,576,600
    player: HCS-KAW
    version: New
    mode: "[1 Player]"
    sources:
      - name: Gamest
        date: "1989-02"
    comments:
      - "517,200 Zenny remaining"

  - score: 8,602,200
    player: KOH
    version: New
    mode: "[1 Player]"
    sources:
      - name: Gamest
        date: "1998-05"

  - score: 8,657,600
    player: KOH
    version: New
    mode: "[1 Player]"
    sources:
      - name: Gamest
        date: "1998-08"

  - score: 8,749,400
    player: KOH
    version: New
    mode: "[1 Player]"
    sources:
      - name: Gamest
        date: "1998-10"

  - score: 11,889,600
    player: EGC-(元)MKZ & (会)IKE
    version: New
    mode: "[2 Player]"
    sources:
      - name: Gamest
        date: "1988-12"

  - score: 11,972,500
    player: EGC-(元)MKZ & (会)IKE
    version: New
    mode: "[2 Player]"
    sources:
      - name: Gamest
        date: "1989-01"

  - score: 12,092,000
    player: ATM・NMTL(ら)難野 & KSTL男-Buchi
    version: New
    mode: "[2 Player]"
    sources:
      - name: Gamest
        date: "1989-07"

  - score: 12,393,100
    player: ATM-NMTL(地)難野 & KSTL Buchi
    version: New
    mode: "[2 Player]"
    sources:
      - name: Gamest
        date: "1989-08"
    comments:
      - "690,000+ Zenny remaining"
      - "10k x26 (stages 4 and 9)"

  - score: 8,665,000
    player: ATARI-KAD
    version: Old
    mode: "[1 Player]"
    sources:
      - name: Gamest
        date: "1988-11"

  - score: 8,767,500
    player: SSC-HST-VAP
    version: Old
    mode: "[1 Player]"
    sources:
      - name: Gamest
        date: "1988-12"
    comments:
      - "432,300 Zenny remaining"

  - score: 8,878,900
    player: 名人(ZKS)
    version: Old
    mode: "[1 Player]"
    sources:
      - name: Gamest
        date: "1989-02"
    comments:
      - "536,000 Zenny remaining"

  - score: 8,892,000
    player: まゆげの武士(かってにウリ坊)
    version: Old
    mode: "[1 Player]"
    sources:
      - name: Gamest
        date: "1989-03"

  - score: 9,051,700
    player: SOF-QUE
    version: Old
    mode: "[1 Player]"
    sources:
      - name: Arcadia
        date: "2004-07"
    comments:
      - "autofire"
      - "Thunder Wind x2"
      - "40k x2"

  - score: 9,177,200
    player: SOF-QUE
    version: Old
    mode: "[1 Player]"
    sources:
      - name: Arcadia
        date: "2004-08"
    comments:
      - "40k x4"

  - score: 9,194,400
    player: SOF-QUE
    version: Old
    mode: "[1 Player]"
    sources:
      - name: Arcadia
        date: "2004-10"
    comments:
      - "autofire"
      - "40k x3"

  - score: 9,250,900
    player: SOF-QUE
    version: Old
    mode: "[1 Player]"
    sources:
      - name: Arcadia
        date: "2005-01"
    attributes:
        is-current-record: true

  - score: 9,264,300
    player: SOF-QUE
    version: Old
    mode: "[1 Player]"
    sources:
      - name: Doujin Superplay VHS
        date: "2005-01"

  - score: 12,158,300
    player: AAC-ABU & GMC-KOM(た)
    version: Old
    mode: "[2 Player]"
    sources:
      - name: Gamest
        date: "1988-11"

  - score: 12,406,700
    player: CRAMP-BOY & HTL-Y.N
    version: Old
    mode: "[2 Player]"
    sources:
      - name: Gamest
        date: "1988-12"

  - score: 13,036,500
    player: SWP-OC-X & POW
    version: Old
    mode: "[2 Player]"
    sources:
      - name: Gamest
        date: "1989-01"

description: "Note: Version differences include a higher difficulty in New Version. Moreover, weapons have been changed, e.g. a weaker Bound Shot in New Version."

links:
  - url: https://shmups.system11.org/viewtopic.php?t=21630
    title: Shmups Forum Hi-Score Topic

layout:
    sort:
        scores:
            version: asc
            mode: asc

translations:
  - property: version
    value: Old
    value-jp: 旧
  - property: version
    value: New
    value-jp: 新
</nowiki></pre>

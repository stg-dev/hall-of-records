'''Attention: This page serves as a database and is not meant to be consumed by end users. If you came here for the STG Hall of Records, follow one of these links:'''

* English version: https://shmups.wiki/library/STG_Hall_of_Records
* 日本語版: https://shmups.wiki/library/STG記録の殿堂

== Games ==

=== Namco Classic Collection Vol.1 ===
<pre><nowiki>
name: "Namco Classic Collection Vol.1"
name-jp: "ナムコクラシックコレクションVol.1"
name-kana: "なむこくらしっくこれくしょんゔぉりゅーむわん"
company: "Namco"
needs-work: true

scores:
  - score: 525,390
    player: Y.F
    game: Galaga (Original)
    sources:
      - name: Gamest
        date: "1996-01"
    comments:
      - "autofire"

  - score: 564,670
    player: GEKI[ぷ](ぬ)
    game: Galaga (Original)
    sources:
      - name: Gamest
        date: "1996-04"
    comments:
      - "autofire"
      - "10k x8"
    attributes:
        is-current-record: true

  - score: 565,420
    player: Jacob Upland
    game: Galaga (Original)
    sources:
      - name: Twin Galaxies
        date: "2020-09-07"
        url: https://www.twingalaxies.com/game/namco-classics-collection-vol-1/mame/points-galaga-original/
      - name: Youtube
        date: "2020-09-07"
        url: https://youtu.be/Eu7QEECD13Q
    comments:
      - "played on Wolfmame"

  - score: 1,390,810
    player: KSG
    game: Galaga (Arrangement)
    manual-sort: 1
    sources:
      - name: Gamest
        date: "1996-01"
    comments:
      - "autofire"

  - score: 1,391,290
    player: KSL-WARRIOR
    game: Galaga (Arrangement)
    manual-sort: 2
    sources:
      - name: Gamest
        date: "1996-11"
    comments:
      - "autofire"

  - score: 1,396,450
    player: KSL-WARRIOR
    game: Galaga (Arrangement)
    manual-sort: 3
    sources:
      - name: Gamest
        date: "1996-12"
    comments:
      - "autofire"
      - "5L remaining"

  - score: 1,396,660
    player: CYR-クイック-WARRIOR
    game: Galaga (Arrangement)
    manual-sort: 4
    sources:
      - name: Gamest
        date: "1997-02"
    comments:
      - "autofire"

  - score: 1,398,560
    player: CYR-KSL-WARRIOR
    game: Galaga (Arrangement)
    manual-sort: 5
    sources:
      - name: Gamest
        date: "1997-03"
    comments:
      - "autofire"

  - score: 1,400,650
    player: KSL-WARRIOR
    game: Galaga (Arrangement)
    manual-sort: 6
    sources:
      - name: Gamest
        date: "1997-04"

  - score: 1,408,690
    player: WARRIOR もうモヒカンはコリゴリ
    game: Galaga (Arrangement)
    manual-sort: 7
    sources:
      - name: Gamest
        date: "1999-08"
    comments:
      - "autofire"

  - score: 1,406,680
    player: CYR-WARてるくはのるFAN
    game: Galaga (Arrangement)
    manual-sort: 8
    sources:
      - name: Arcadia
        date: "2000-06"
    comments:
      - "score lower than what was previously submitted in Gamest issue 1999-08"
      - "autofire"
      - "5L remaining"

  - score: 1,414,450
    player: MOL=CKDF=ふるえんも
    game: Galaga (Arrangement)
    manual-sort: 9
    sources:
      - name: Arcadia
        date: "2005-01"

  - score: 1,416,870
    player: CYR-WAR
    game: Galaga (Arrangement)
    manual-sort: 10
    sources:
      - name: Arcadia
        date: "2010-10"
    comments:
      - "autofire"

  - score: 1,421,290
    player: CYR-WAR
    game: Galaga (Arrangement)
    manual-sort: 11
    sources:
      - name: Arcadia
        date: "2011-01"
    comments:
      - "autofire"

  - score: 267,900
    player: AXIOM-H.Y(奈)
    game: Super Xevious (Original)
    sources:
      - name: Gamest
        date: "1996-01"
    comments:
      - "autofire"
      - "died area 12"

  - score: 364,090
    player: JR.
    game: Super Xevious (Original)
    sources:
      - name: Gamest
        date: "1996-04"
    comments:
      - "autofire"
      - "died area 16"

  - score: 839,390
    player: NeoEX-EL2(戦術エース)
    game: Super Xevious (Original)
    sources:
      - name: Gamest
        date: "1996-05"
    comments:
      - "autofire"
      - "3L remaining"

  - score: 913,100
    player: 吉田サンのおかげです
    game: Super Xevious (Original)
    sources:
      - name: Gamest
        date: "1997-03"
    comments:
      - "SP x4"
      - "4L remaining"

  - score: 1,010,790
    player: HOP-KTL-EL2(戦術エース)
    game: Super Xevious (Original)
    sources:
      - name: Gamest
        date: "1997-06"

  - score: 1,234,060
    player: KTL-EL2(戦術エース3)
    game: Super Xevious (Original)
    sources:
      - name: Gamest
        date: "1997-08"
    comments:
      - "autofire"

  - score: 1,525,460
    player: KTL-EL2
    game: Super Xevious (Original)
    sources:
      - name: Gamest
        date: "1997-09"

  - score: 1,650,910
    player: JMB-りゅう(2nd)
    game: Super Xevious (Original)
    sources:
      - name: Arcadia
        date: "2011-04"
    comments:
      - "autofire"
      - "0L remaining"

  - score: 2,017,560
    player: JMB-BETTER
    game: Super Xevious (Original)
    sources:
      - name: Arcadia
        date: "2011-05"
    comments:
      - "autofire"
      - "SP x4"
      - "0L remaining"

  - score: 2,497,240
    player: ナポ椎名氏池田氏せたろ～氏に感謝! JMB
    game: Super Xevious (Original)
    sources:
      - name: Arcadia
        date: "2011-11"
    comments:
      - "autofire (synchronized 30hz)"
      - "SP x4"
      - "clear bonus 100k"
      - "0L remaining"

  - score: 2,593,700
    player: ナポに来てくれた方々に感謝です。 JMB
    game: Super Xevious (Original)
    sources:
      - name: Arcadia
        date: "2012-07"
    comments:
      - "autofire"
      - "SP x4"
      - "0L remaining"

  - score: 4,312,290
    player: K.N尊師
    game: Super Xevious (Original)
    sources:
      - name: JHA
        date: "2016-10"
    comments:
      - "autofire"

  - score: 1,181,860
    player: Mr.さくやま君
    game: Xevious (Original)
    sources:
      - name: Gamest
        date: "1996-01"
    comments:
      - "autofire"

  - score: 1,366,350
    player: MOI
    game: Xevious (Original)
    sources:
      - name: Gamest
        date: "1996-03"
    comments:
      - "no miss"
      - "SP x4"

  - score: 1,384,860
    player: PSD-KTL-KAA(川村)
    game: Xevious (Original)
    sources:
      - name: Gamest
        date: "1996-06"
    comments:
      - "autofire"
      - "no miss"

  - score: 1,401,710
    player: HOP-KTL-EL2
    game: Xevious (Original)
    sources:
      - name: Gamest
        date: "1997-07"
    comments:
      - "autofire"
      - "7L remaining"

  - score: 1,409,230
    player: KTL-EL2(戦国ブレード)
    game: Xevious (Original)
    sources:
      - name: Gamest
        date: "1997-08"
    comments:
      - "autofire"
      - "no miss"

  - score: 1,443,390
    player: KTL-EL2
    game: Xevious (Original)
    sources:
      - name: Gamest
        date: "1997-09"

  - score: 1,474,350
    player: CYR-せたろ～さんに感謝! JMB-RR
    game: Xevious (Original)
    sources:
      - name: Arcadia
        date: "2011-05"
    comments:
      - "Hard (default)"
      - "SP x4"

  - score: 1,525,620
    player: ずばさん元気にしてますか? JMB魂!
    game: Xevious (Original)
    sources:
      - name: Arcadia
        date: "2011-08"
    comments:
      - "SP x4"

  - score: 1,528,810
    player: 名古屋のTORさん達人王頑張って! JMB
    game: Xevious (Original)
    sources:
      - name: Arcadia
        date: "2011-12"
    comments:
      - "autofire (synchronized 30hz)"
      - "SP x4"
      - "no miss"

  - score: 1,578,070
    player: ずばさん復活おめでとう! 希望の轍 JMB
    game: Xevious (Original)
    sources:
      - name: Arcadia
        date: "2012-01"
    comments:
      - "autofire (synchronized 30hz)"
      - "SP x4"

  - score: 1,600,720
    player: LIB氏 ひろりん氏 地獄龍H.S氏に超感謝
    game: Xevious (Original)
    sources:
      - name: Arcadia
        date: "2012-02"
    comments:
      - "SP x4"

  - score: 1,713,080
    player: K.N尊師
    game: Xevious (Original)
    sources:
      - name: JHA
        date: "2016-11"

  - score: 2,658,160
    player: CHAT-ああるじーぶー
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1996-01"
    comments:
      - "autofire"
      - "died in Extra 1"

  - score: 5,577,590
    player: G.M.C.R-I
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1996-02"
    comments:
      - "autofire"
      - "no miss"
      - "Extra 3 clear"

  - score: 5,632,510
    player: G.M.C.R-I
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1996-03"
    comments:
      - "autofire"
      - "Extra 3 clear"

  - score: 5,843,750
    player: KUMAMOTO
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1996-05"
    comments:
      - "autofire"
      - "Extra 3 clear"
      - "1up x1"

  - score: 5,990,780
    player: KUMAMOTO
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1996-10"
    comments:
      - "autofire"

  - score: 6,013,920
    player: KUMAMOTO
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1996-12"
    comments:
      - "autofire"
      - "Extra 3 clear"

  - score: 6,147,840
    player: KUMAMOTO
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1997-03"
    comments:
      - "autofire"
      - "Extra 3 clear"

  - score: 6,166,210
    player: 夏コミもE古田シューティングチーム-(め)
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1997-06"

  - score: 6,345,160
    player: 夏コミは金曜東地図エー56a めぞん
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1997-08"
    comments:
      - "after area 16 3.35m"
      - "Extra 3 clear"

  - score: 6,529,970
    player: ALT-AKI
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1997-12"

  - score: 6,754,640
    player: ALT-AKI
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1998-01"

  - score: 6,788,930
    player: "夏コミは、金曜東地区リ-35a/(め)"
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Gamest
        date: "1998-08"

  - score: 7,003,820
    player: SPK(お)徳田さんのおかげです。
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Arcadia
        date: "2004-12"
    comments:
      - "before area 16 3.53m"
      - "Extra 3 suicide x1"

  - score: 7,138,120
    player: 白川, 熊本, じーぶー菊間, なかっぴ, 徳田
    game: Xevious (Arrangement)
    mode: Normal
    sources:
      - name: Arcadia
        date: "2009-11"
    comments:
      - "30hz autofire"
      - "after area 16 3.66m"
      - "Extra 3 clear"

  - score: 7,545,510
    player: RVS-R.Y氏H.T氏に感謝してます!
    game: Xevious (Arrangement)
    mode: Mania
    sources:
      - name: Arcadia
        date: "2011-01"
    comments:
      - "after area 16 4.1m"
      - "from Extra 1 to Extra 3 1.27m"
      - "Extra 3 suicide x4: 1.16m"

  - score: 7,596,250
    player: ジャンボの皆様に感謝BetterDays
    game: Xevious (Arrangement)
    mode: Mania
    sources:
      - name: Arcadia
        date: "2011-03"
    comments:
      - "autofire"
      - "Extra 3 clear"

  - score: 7,649,410
    player: JMB-IKA氏お世話にな
    game: Xevious (Arrangement)
    mode: Mania
    sources:
      - name: Arcadia
        date: "2012-03"
    comments:
      - "autofire"

  - score: 7,754,380
    player: JMB-りゅう師匠, IKA師匠に感謝。 RR
    game: Xevious (Arrangement)
    mode: Mania
    sources:
      - name: Arcadia
        date: "2012-05"

description: "Note: Originally, the Japanese scoreboard for Xevious (Arrangement) only consisted of one category. The category was split into 'Normal Mode' and 'Mania Mode' in JHA issue September 2019. [https://twitter.com/JHA_information/status/1173528231053418496 Read the official statement here]. To enter Mania Mode (max rank), the player has to fulfill three conditions: 1) activate hidden message in area 1 (bomb a certain section in the trees); 2) uncover both sols in area 1; 3) activate hidden message in area 2 (bomb a certain section in the trees)."

links:
  - url: https://wiki.denfaminicogamer.jp/highscore/1995_014_%E3%83%8A%E3%83%A0%E3%82%B3%E3%82%AF%E3%83%A9%E3%82%B7%E3%83%83%E3%82%AF%E3%82%B3%E3%83%AC%E3%82%AF%E3%82%B7%E3%83%A7%E3%83%B3Vol.1
    title: JHA Leaderboard
  - url: https://shmups.system11.org/viewtopic.php?t=39648
    title: Shmups Forum Hi-Score Topic (Galaga)
  - url: https://shmups.system11.org/viewtopic.php?t=39729
    title: Shmups Forum Hi-Score Topic (Xevious)

layout:
    sort:
        scores:
            game: [ Galaga (Original), Galaga (Arrangement), Super Xevious (Original), Xevious (Original), Xevious (Arrangement) ]
            mode: desc
            manual-sort: desc

translations:
  - property: game
    value: Galaga (Original)
    value-jp: ギャラガ・オリジナル
  - property: game
    value: Galaga (Arrangement)
    value-jp: ギャラガ・アレンジ
  - property: game
    value: Super Xevious (Original)
    value-jp: スーパーゼビウス・オリジナル
  - property: game
    value: Xevious (Original)
    value-jp: ゼビウス・オリジナル
  - property: game
    value: Xevious (Arrangement)
    value-jp: ゼビウス・アレンジ
  - property: mode
    value: Normal
    value-jp: ノーマル
  - property: mode
    value: Mania
    value-jp: マニア
</nowiki></pre>

=== Nebulas Ray ===
<pre><nowiki>
name: "Nebulas Ray"
name-jp: ネビュラスレイ
name-kana: ねびゅらすれい
company: "Namco"
needs-work: true

scores:
  - score: 2,610,260
    player: MFC-軍曹(天)
    autofire: -
    sources:
      - name: Gamest
        date: "1994-06-15"
    attributes:
        is-current-record: false

  - score: 2,690,530
    player: A-10
    autofire: OFF
    sources:
      - name: Gamest
        date: "1994-11-15"

  - score: 2,825,320
    player: A.BOY
    autofire: OFF
    sources:
      - name: Twitter
        date: "2022-03-21"
        url: https://twitter.com/hiropom_76/status/1505931279459848194
      - name: JHA
        date: "2022-03"
      - name: Youtube
        date: "2022-03-22"
        url: https://youtu.be/aNOc7qoel6A
    comments:
      - "stage 5 bonus x20"

  - score: 2,854,200
    player: A.BOY
    autofire: OFF
    sources:
      - name: Twitter
        date: "2022-03-27"
        url: https://twitter.com/hiropom_76/status/1508060795724509191

  - score: 2,873,370
    player: A.BOY
    autofire: OFF
    sources:
      - name: Twitter
        date: "2022-04-03"
        url: https://twitter.com/hiropom_76/status/1510600898791505920
      - name: Youtube
        date: "2022-04-04"
        url: https://youtu.be/waPEsl9Jn2U

  - score: 2,901,810
    player: A.BOY
    autofire: OFF
    sources:
      - name: Twitter
        date: "2022-04-17"
        url: https://twitter.com/hiropom_76/status/1515644827538366467
      - name: JHA
        date: "2022-04"
      - name: Youtube
        date: "2022-04-18"
        url: https://youtu.be/GOlSrSJxZRQ
    comments:
      - "1 death at 2-1 boss"

  - score: 2,878,750
    player: 京城
    autofire: ON
    sources:
      - name: Gamest
        date: "1994-06-30"

  - score: 2,898,890
    player: 京城
    autofire: ON
    sources:
      - name: Gamest
        date: "1994-09-30"

  - score: 2,905,130
    player: JAG-WOP
    autofire: ON
    sources:
      - name: Gamest
        date: "1994-11-15"

  - score: 2,919,720
    player: ムッシュかまやつ
    autofire: ON
    sources:
      - name: Gamest
        date: "1994-11-30"

  - score: 2,933,070
    player: hamami
    autofire: ON
    sources:
      - name: Twitter
        date: "2014-10-17"
        url: https://twitter.com/hamami999/status/523105331002363905
      - name: Arcadia
        date: "2015-01"

  - score: 2,953,560
    player: hamami
    autofire: ON
    sources:
      - name: Twitter
        date: "2014-11-06"
        url: https://twitter.com/hamami999/status/530296390769856512
      - name: Arcadia
        date: "2015-02"

  - score: 2,960,660
    player: hamami
    autofire: ON
    sources:
      - name: Twitter
        date: "2014-12-26"
        url: https://twitter.com/hamami999/status/548432063947218944
      - name: Arcadia
        date: "2015-04"

  - score: 2,981,790
    player: hamami
    autofire: ON
    sources:
      - name: Twitter
        date: "2015-02-05"
        url: https://twitter.com/hamami999/status/563284194336333825
      - name: JHA
        date: "2016-03"

  - score: 3,014,420
    player: A.BOY
    autofire: ON
    sources:
      - name: Twitter
        date: "2021-03-21"
        url: https://twitter.com/hiropom_76/status/1373636672781389834
      - name: JHA
        date: "2021-02"

links:
  - url: https://wiki.denfaminicogamer.jp/highscore/1994_005_%E3%83%8D%E3%83%93%E3%83%A5%E3%83%A9%E3%82%B9%E3%83%AC%E3%82%A4
    title: JHA Leaderboard
  - url: https://shmups.system11.org/viewtopic.php?t=42761
    title: Shmups Forum Hi-Score Topic

layout:
    sort:
        scores:
            autofire: asc
</nowiki></pre>

=== Night Raid ===
<pre><nowiki>
name: "Night Raid"
name-jp: "ナイトレイド"
name-kana: "ないとれいど"
company: "Takumi"
needs-work: true

scores:
  - score: 5,443,646,580
    player: 3億落ち。征木
    mode: Highest Score
    sources:
      - name: Arcadia
        date: "2001-09"

  - score: 5,641,656,150
    player: クソゲー。征木二等兵
    mode: Highest Score
    sources:
      - name: Arcadia
        date: "2001-10"

  - score: 5,840,904,800
    player: まだ続けたい KNN-はんばーぐみちる
    mode: Highest Score
    sources:
      - name: Arcadia
        date: "2001-11"
    comments:
      - "autofire"

  - score: 5,900,202,120
    player: KNN-新宿ナイトレイダー
    mode: Highest Score
    sources:
      - name: Arcadia
        date: "2001-12"

  - score: 5,951,396,100
    player: KMB(完全無欠のバティンボ)
    mode: Highest Score
    sources:
      - name: Arcadia
        date: "2011-02"
    comments:
      - "autofire A"
      - "stage 1: 640m; stage 2: 1.41b; stage 3: 3.02b; stage 4: 3.77b; stage 5: 4.79b; before last boss: 5.25b"
      - "0L remaining"

  - score: 6,085,692,070
    player: KMB(バティンボ大好きキメラにゃん)
    mode: Highest Score
    sources:
      - name: Arcadia
        date: "2011-03"
    comments:
      - "autofire A"
      - "0L remaining"

  - score: 6,289,870,260
    player: KMB(画イ汁くんに後は任せた)
    mode: Highest Score
    sources:
      - name: Arcadia
        date: "2011-04"
      - name: Niconico
        date: "2011-12-29"
        url: https://www.nicovideo.jp/watch/sm16561255
    comments:
      - "autofire A & B"
      - "stage 1: 711m; stage 2: 1.523b; stage 3: 3.395b; stage 4: 4.162b; stage 5: 5.179b"
      - "0L remaining"

  - score: 6,428,538,520
    player: KMB(ナイトレイダー募集)
    mode: Highest Score
    sources:
      - name: Arcadia
        date: "2011-05"
    comments:
      - "autofire A & B"
      - "0L remaining"

  - score: "0"
    player: 全国103TENのみなさま(偽)
    mode: Near Zero
    sources:
      - name: Arcadia
        date: "2001-09"
    comments:
      - "autofire"

links:
  - url: https://wiki.denfaminicogamer.jp/highscore/2001_034_%E3%83%8A%E3%82%A4%E3%83%88%E3%83%AC%E3%82%A4%E3%83%89
    title: JHA Leaderboard
  - url: https://shmups.system11.org/viewtopic.php?t=22165
    title: Shmups Forum Hi-Score Topic

layout:
    sort:
        scores:
            mode: asc

translations:
  - property: mode
    value: Highest Score
    value-jp: プラススコア
  - property: mode
    value: Near Zero
    value-jp: ニアゼロ
</nowiki></pre>

=== Nostradamus ===
<pre><nowiki>
name: "Nostradamus"
name-jp: "ノストラダムス"
name-kana: "のすとらだむす"
company: "Face"
counterstop:
  - score: 9,999,900
    type: hard
needs-work: true

scores:
  - score: 9,999,900
    player: 通称君こそ天王山決戦 (大)(宮)(お)(た)(く) REO
    mode: 1P
    sources:
      - name: Gamest
        date: "1993-12"
    comments:
      - "autofire"
      - "no miss"

  - score: 9,999,900
    player: G.M.C.R-I(ベ)
    mode: 2P
    sources:
      - name: Gamest
        date: "1993-12"
    comments:
      - "autofire"

description: "Note: Scoreboard closed after the achievement of the counterstop at 9,999,900."

links:
  - url: https://shmups.system11.org/viewtopic.php?t=57781
    title: Shmups Forum Hi-Score Topic

layout:
    columns:
        mode:
            label: Player Side
            label-jp: 側
</nowiki></pre>

<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Game;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<\App\Models\Game>
 */
final class GameFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Game::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name,
            'name_jp' => fake()->optional()->word,
            'name_kana' => fake()->optional()->word,
            'company_id' => \App\Models\Company::factory(),
            'counterstop_score' => fake()->optional()->word,
            'counterstop_type' => fake()->word,
            'needs_work' => fake()->boolean,
            'description' => fake()->optional()->text,
        ];
    }
}

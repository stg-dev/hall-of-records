<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Score;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<\App\Models\Score>
 */
final class ScoreFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Score::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'game_id' => \App\Models\Game::factory(),
            'score' => fake()->word,
            'player' => fake()->word,
            'mode' => fake()->optional()->word,
            'links' => '{}',
            'sources' => '{}',
            'comments' => '{}',
        ];
    }
}

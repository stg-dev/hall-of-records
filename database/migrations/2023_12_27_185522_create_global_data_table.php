<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration
{
    public function up(): void
    {
        Schema::create('global_data', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('data_key');
            $table->text('data_value');
        });
    }
};

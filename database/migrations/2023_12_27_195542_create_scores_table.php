<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration
{
    public function up(): void
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('game_id')->constrained();
            $table->foreignId('category_id')->constrained();
            $table->foreignId('player_id')->nullable()->constrained();
            $table->string('entry_name')->nullable();
            $table->string('score');
            $table->text('yaml_data')->nullable();
        });
    }
};

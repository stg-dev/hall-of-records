MAKEFLAGS=-s

help:
	echo "Possible commands are:\n"
	cat ${MAKEFILE_LIST} | grep -v "{MAKEFILE_LIST} | grep -v" | grep ":.*##" | column -t -s "##"

install: ## install hall-of-records
	cp .env .env.orig || true # backup possibly existing previous .env file
	cp .env.example .env

	# @link https://stackoverflow.com/questions/38483837/please-provide-a-valid-cache-path-error-in-laravel
	mkdir -p storage/app storage/framework storage/framework/cache storage/framework/views storage/framework/sessions

	 # ignore reqs, get sail running whatever php version the host has running
	composer install --ignore-platform-reqs
	./vendor/bin/sail down
	./vendor/bin/sail up -d
	./vendor/bin/sail artisan key:generate

	echo "sleeping 15 secs to give DB time to initialize and start up..."
	sleep 15
	./vendor/bin/sail artisan migrate:fresh
	./vendor/bin/sail artisan db:seed
	./vendor/bin/sail bun install

	./vendor/bin/sail bun run dev

inspect:
	vendor/bin/phpstan analyse --memory-limit=512M --level=6 app config resources tests
	vendor/bin/phpstan analyse --memory-limit=513M --level=0 database
	vendor/bin/pint --test

deploy:
	rm -rf deployment.log temp
	vendor/bin/sail bun install
	vendor/bin/sail bun run build
	vendor/bin/sail composer install --no-dev
	php deployment.phar deployment.php
	composer install --ignore-platform-reqs
